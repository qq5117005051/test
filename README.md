Create a commerce v9 cloud environment with jenkins
=======================================================

###### //commerce v9 environment introduction here
You could create a v9 environment by following steps:
***
## 1. Create a group for your own cloud pipeline.

This session is in order to create a jenkins group and copy pipelines from templates for your own environment. You could complete other steps with these pipeline in this new group. 

### 1) Login as admin with admin/admin as username/password. The jenkis url is <http://9.111.213.184:9001>
### 2) Change to New Pipeline group, and run `CreateGroup_Base` pipeline, and input your cloud name in the `group_id` field.

Then you could find a new view in jenkins named `group_id` you just input before.

## ***Optinal Steps*** :Make jenkins display special view for every user
As usual, we hope users could only manage their own jenkins jobs or pipelines, so admin must configure the global security to ensure this.

### Prepariton: Create a group named *test* as an example. The new pipelines will be created naming xxx_test
###1) Create a new user named *client* as the owner of the group *test* in “系统管理 >> 管理用户 >> 新建用户”.

![image](https://gitlab.com/qq5117005051/test/blob/master/123.png)

###2) Install plugin "Role-based Authorization Strategy" in “系统管理 >> 管理插件 >> 可选插件”.

###3) Configure Global Security to "Role-Based Strategy" in "system configuration >> Configure Global Security"

###4) Create a rule named *test* to filter pipelines created before in "系统管理 >> Manage and Assign Roles >> Manage Roles >> Project Roles".

###5) Add the item roles *test* to the user *client*